
*********
Sequencer
*********

The Sequencer view type shows a timeline (highlighted in red in the figure below).

.. figure:: /images/editors_vse_sequencer.svg
   :alt: Sequencer

   Components of the Sequencer

.. toctree::
   :maxdepth: 2

   introduction.rst
   channels.rst
   navigating.rst
   display.rst
   toolbar/index.rst
   sidebar/index.rst
